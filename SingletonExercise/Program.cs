﻿using System;

namespace SingletonExercise
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var highScore = new HighScore();
            highScore.UpdateHighScore("Jack", 1000);

            var highScore2 = new HighScore();
            highScore2.UpdateHighScore("Jill", 2000);

            Console.WriteLine($"High score: {highScore.PlayerName} {highScore.Score}");
        }
    }
}
