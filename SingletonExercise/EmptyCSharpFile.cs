﻿namespace SingletonExercise 
{
    class HighScore
    {

        public string PlayerName { get; private set; }
        public int Score { get; private set; }

        public void UpdateHighScore(string name, int score)
        {
            if (score > Score) {
                Score = score;
                PlayerName = name;
            }
        }
    }
}